<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 8/14/15
 * Time: 12:12 PM
 */

class DC_Demo_IndexController extends Mage_Core_Controller_Front_Action{

    public function indexAction(){
        $this->loadLayout();
        $this->renderLayout();
    }
}